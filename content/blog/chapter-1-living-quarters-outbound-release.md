---
title: "Chapter 1: Living Quarters Outbound Release"
desc: "Initially developed and released by Gearbox Software in 2001, second Half-Life add-on, Half-Life: Blue Shift is a return to the Black Mesa Research Facility and another look at the Incident's consequences from the eyes of Gordon Freeman's best friend - Barney Calhoun"
image:
  img: img/uploads/map1-cafe_1080.jpg
  alt: "Chapter 1: Living Quarters Outbound Release"
date: 2021-03-24T16:14:23.619Z
blocks:
  - type: textBlock
    text: >-
      Initially developed and released by Gearbox Software in 2001,
      second Half-Life add-on, Half-Life: Blue Shift is a return to the
      Black Mesa Research Facility and another look at the Incident's
      consequences from the eyes of Gordon Freeman's best friend - Barney
      Calhoun


      In 2012, Crowbar Collective has delighted Half-Life fans with release of the magnificent Half-Life remake, but there are still no released remakes for add-ons. This is when we are coming in - HECU Collective are developing the Black Mesa: Blue Shift - a free remake with use of Black Mesa resources and style. We will try to stay as close to the original Blue Shift and Black Mesa as possible. Our mod will be releasing partially, chapter by chapter, so those who are not patient for the full release will finally have something to play!
  - type: titleBlock
    value:
      level: 3
      title: "Our composer, Daver, have released his third track for this chapter on Youtube:"
  - type: youtubeBlock
    id: WBhejZNBve0
  - type: listLinks
    value:
      - link: https://vk.com/hecuteam
        text: Vkontakte
      - link: https://discord.gg/YYTYHJq3XB
        text: Discord
      - link: https://www.youtube.com/channel/UCjoL6M9_8-93vmxjO4W5R9w
        text: Daver's Youtube Channel
  - type: textImageBlock
    img: img/uploads/20180327231512_1.jpg
    alt: The Page dot
    text: >-
      ## The Page dot.


      The root context, the one available to you in your `baseof.html` and layouts will always be the Page context. Basically everything you need to build this page is in that dot.\

      `.Title`, `.Permalink`, `.Resources`, you name it.


      Even your site’s informations is stored in the page context with `.Site` ready for the taking.


      But in Go Template the minute you step into a function you lose that context as your precious dot or context is replaced by the function’s own… dot.


      Let’s dive into a template file!
    reverse: true
  - type: textImageBlock
    img: img/uploads/asheep_800.jpg
    alt: It will be 2 years soon
    text: >-
      It will be 2 years soon. During this time, there were ups and downs of
      the modification. But is it otherwise development has been and is
      going on now. In addition, one good section of the mod helped me make
      my good friend.Now we turn specifically to the modification. During
      the optimization, it was decided to abandon some locations and thus
      there will be only 3 maps - a, b, c, but the mod will be quite large,
      with rich gameplay.


      At the moment, the availability of the maps is as follows:


      * A - 90%

      * B - 80%

      * C - 60%


      My mistake was on moddb to set the release date for the 4th quarter of 2019. Obviously, development will flow into 2020. So, while the status is “To be announced”


      I want to show screenshots of their Hammer’a, while there is no way to get a good picture. But testing passes, albeit without light with fullbright’s))
    reverse: false
  - type: htmlBlock
    code:
      code: '<a href="https://www.moddb.com/mods/power-pit" title="View Black Mesa:
        Power pit on Mod DB" target="_blank"><img
        src="https://button.moddb.com/popularity/medium/mods/35838.png"
        alt="Black Mesa: Power pit" /></a>'
  - type: imageBlock
    img: /img/uploads/20180327231512_1.jpg
    alt: image-block
---
